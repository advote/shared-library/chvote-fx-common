/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-fx-common                                                                               -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.javafx.business.exception;

/**
 * An unchecked exception that includes an error code in addition to the message and the cause of the exception. The
 * error code is composed of the exception class simple name and the error code suffix provided in the constructor.
 */
public abstract class ErrorCodeBasedException extends RuntimeException {
  private static final String ERROR_CODE_FORMAT = "%s.%s";

  private final String errorCode;

  /**
   * Create a new {@link ErrorCodeBasedException} for the specified error code.
   *
   * @param errorCodeSuffix an error code describing the nature of the exception.
   * @param message         the detail message.
   *
   * @see RuntimeException#RuntimeException(String)
   */
  public ErrorCodeBasedException(String errorCodeSuffix, String message) {
    this(errorCodeSuffix, message, null);
  }

  /**
   * Create a new {@link ErrorCodeBasedException} for the specified error code.
   *
   * @param errorCodeSuffix an error code describing the nature of the exception.
   * @param message         the detail message.
   * @param cause           the cause of this exception.
   *
   * @see RuntimeException#RuntimeException(String, Throwable)
   */
  public ErrorCodeBasedException(String errorCodeSuffix, String message, Throwable cause) {
    super(message, cause);
    this.errorCode = String.format(ERROR_CODE_FORMAT, getClass().getSimpleName(), errorCodeSuffix);
  }

  /**
   * Get the error code of this exception. The error code is composed by this class simple name and the error code
   * suffix provided in the constructor, i.e.: {@code getClass().getSimpleName() + "." + errorCodeSuffix}.
   *
   * @return the formatted error code.
   *
   * @see ErrorCodeBasedException#ErrorCodeBasedException(String, String)
   * @see ErrorCodeBasedException#ErrorCodeBasedException(String, String, Throwable)
   */
  public String getErrorCode() {
    return errorCode;
  }
}
