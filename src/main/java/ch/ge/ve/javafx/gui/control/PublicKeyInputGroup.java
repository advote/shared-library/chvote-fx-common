/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-fx-common                                                                               -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.javafx.gui.control;

import ch.ge.ve.javafx.gui.utils.FXMLLoaderUtils;
import ch.ge.ve.javafx.gui.utils.FileBrowserService;
import com.jfoenix.controls.JFXTextField;
import java.nio.file.Path;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.scene.layout.VBox;

/**
 * Base JavaFX Controller for selecting the destination folder of a public key.
 */
public class PublicKeyInputGroup extends VBox {
  private static final String DEFAULT_STYLE_CLASS = "chvote-public-key-input-group";

  @FXML
  private MessageContainer errorMessageContainer;

  @FXML
  private JFXTextField destinationPathTextField;

  private final ObjectProperty<FileBrowserService> fileBrowserService;
  private final ObjectProperty<Path>               destinationPath;

  /**
   * Create a new public key input group, containing a {@link JFXTextField} for the destination folder.
   */
  public PublicKeyInputGroup() {
    this(new FileBrowserService());
  }

  /**
   * Create a new public key input group containing the following children:
   * <ul>
   * <li>A {@link JFXTextField} for the destination folder.</li>
   * </ul>
   *
   * @param fileBrowserService the file browser service that will be used to select the destination folder.
   */
  public PublicKeyInputGroup(FileBrowserService fileBrowserService) {
    this.fileBrowserService = new SimpleObjectProperty<>(this, "fileBrowserService");
    this.destinationPath = new SimpleObjectProperty<>(this, "destinationPath");

    FXMLLoaderUtils.load(this, "/view/PublicKeyInputGroup.fxml");

    setFileBrowserService(fileBrowserService);

    initialize();
  }

  private void initialize() {
    getStyleClass().add(DEFAULT_STYLE_CLASS);
  }

  /**
   * Open the file chooser dialog to select a destination folder for the generated public key.
   */
  @FXML
  public final void selectDestinationFolder() {
    destinationPath.set(getFileBrowserService().selectDirectory(destinationPathTextField));
  }

  /**
   * Validate that the destination path is not empty.
   *
   * @return true if the value is valid else false
   */
  public final boolean validate() {
    return destinationPathTextField.validate();
  }

  /**
   * Clear all the fields in this input group.
   */
  public final void clear() {
    destinationPathTextField.clear();
    destinationPath.set(null);
    errorMessageContainer.dismiss();
  }

  /*--------------------------------------------------------------------------
   - Properties                                                              -
   ---------------------------------------------------------------------------*/

  /**
   * Get the file browser service used to select the destination folder.
   *
   * @return the file browser service.
   */
  public final FileBrowserService getFileBrowserService() {
    return fileBrowserService.get();
  }

  /**
   * Set the file browser service that will be used to select the destination folder.
   *
   * @param value the file browser service.
   */
  public final void setFileBrowserService(FileBrowserService value) {
    fileBrowserService.setValue(value);
  }

  /**
   * The file browser service property.
   *
   * @return the file browser service property.
   */
  public final ObjectProperty<FileBrowserService> fileBrowserServiceProperty() {
    return fileBrowserService;
  }

  /**
   * Get the destination path set by the user. This function can return an invalid destination path, call {@link
   * #validate()} before retrieving this value to check for validity.
   *
   * @return the destination path.
   */
  public final Path getDestinationPath() {
    return destinationPath.get();
  }

  /**
   * The text property of the destination path field.
   *
   * @return the text property of the destination path field
   */
  public final ObjectProperty<Path> destinationPathProperty() {
    return destinationPath;
  }

  /**
   * Get the current error message in the message container of this input group.
   *
   * @return the current error message.
   */
  public final String getErrorMessage() {
    return errorMessageContainer.getMessage();
  }

  /**
   * Set the error message in the message container. This will override the previous message if present.
   *
   * @param value the new error message.
   */
  public final void setErrorMessage(String value) {
    errorMessageContainer.setMessage(value);
  }

  /**
   * The text property of the error message.
   *
   * @return the text property of the error message.
   */
  public final StringProperty errorMessageProperty() {
    return errorMessageContainer.messageProperty();
  }
}