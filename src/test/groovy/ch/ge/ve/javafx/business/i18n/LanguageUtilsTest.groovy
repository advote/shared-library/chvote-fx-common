/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-fx-common                                                                               -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.javafx.business.i18n

import spock.lang.Specification

class LanguageUtilsTest extends Specification {

  private static final Locale initialLocale = Locale.getDefault() // so that we can reset to that after the tests

  void cleanupSpec() {
    Locale.setDefault(initialLocale)
  }

  def "getDefaultLocale should return only supported languages"() {
    given:
    LanguageUtils.setCurrentLocale(defaultLocale)

    when:
    def locale = LanguageUtils.getCurrentLocale()

    then:
    locale == expectedLocale

    where:
    defaultLocale  | expectedLocale
    Locale.ENGLISH | Locale.FRENCH
    Locale.FRENCH  | Locale.FRENCH
    Locale.GERMAN  | Locale.GERMAN
  }

}
