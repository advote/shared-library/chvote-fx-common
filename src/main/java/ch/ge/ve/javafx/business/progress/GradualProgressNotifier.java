/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-fx-common                                                                               -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.javafx.business.progress;

import java.util.concurrent.atomic.AtomicLong;

/**
 * A component that operates a {@link ProgressTracker} to gradually add value to reach a defined total number.
 */
public class GradualProgressNotifier {

  private final ProgressTracker progressTracker;
  private final long            total;
  private final AtomicLong      current;

  /**
   * Create a new notifier, delegating the progress tracking to a {@link ProgressTracker} instance.
   *
   * @param progressTracker to tracker to notify of the progress
   * @param total           the total number of (whatever) to reach
   */
  public GradualProgressNotifier(ProgressTracker progressTracker, long total) {
    this.progressTracker = progressTracker;
    this.total = total;
    this.current = new AtomicLong();
  }

  /**
   * Notify that progress has been made.
   *
   * @param gain the number to add to the current "position"
   */
  public void notifyProgress(long gain) {
    long progress = current.addAndGet(gain);
    progressTracker.updateProgress(progress, total);
  }
}
