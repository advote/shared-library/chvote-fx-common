/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-fx-common                                                                               -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.javafx.business.progress;

import java.util.concurrent.atomic.AtomicLong;

/**
 * A component that operates a {@link ProgressTracker} by incrementing the progress one unit at a time.
 */
public class IncrementalProgressNotifier {
  private final ProgressTracker tracker;
  private final long            total;
  private final AtomicLong      current;

  /**
   * Create a new {@link IncrementalProgressNotifier} instance.
   *
   * @param tracker the decorated tracker.
   * @param total   the total amount of increments.
   */
  public IncrementalProgressNotifier(ProgressTracker tracker, long total) {
    this.tracker = tracker;
    this.total = total;
    this.current = new AtomicLong();
    tracker.updateProgress(current.get(), total);
  }

  /**
   * Increment one unit the progress of the underlying {@link ProgressTracker}.
   */
  public void increment() {
    tracker.updateProgress(current.incrementAndGet(), total);
  }

}
