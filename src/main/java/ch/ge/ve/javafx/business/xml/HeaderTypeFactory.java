/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-fx-common                                                                               -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.javafx.business.xml;

import ch.ge.ve.interfaces.ech.eCH0058.v5.HeaderType;
import ch.ge.ve.interfaces.ech.eCH0058.v5.SendingApplicationType;
import java.time.LocalDateTime;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * A factory to create common ECH "HeaderType" header objects.
 */
@Component
public class HeaderTypeFactory {

  private static final String HEADER_ACTION = "1";

  private final String manufacturer;
  private final String productName;
  private final String productVersion;

  private final String  messageType;
  private final boolean testFlag;

  /**
   * Fully-specified constructor.
   * <p>
   * Identifies the product that creates the message, the declared type for the message, and the "testDeliveryFlag"
   * (defines whether a message has been created for test purposes only...).
   * </p>
   *
   * @param manufacturer   the creator of the product
   * @param productName    a given name for the product
   * @param productVersion version (10 chars max)
   * @param messageType    type of the message
   * @param testFlag       messages created for test only ?
   */
  @Autowired
  public HeaderTypeFactory(@Value("${header.product.manufacturer}") String manufacturer,
                           @Value("${header.product.name}") String productName,
                           @Value("${header.product.version}") String productVersion,
                           @Value("${header.message.type}") String messageType,
                           @Value("${header.test.delivery.flag:false}") boolean testFlag) {
    this.manufacturer = manufacturer;
    this.productName = productName;
    this.productVersion = productVersion;
    this.messageType = messageType;
    this.testFlag = testFlag;
  }

  /**
   * A "simplified" constructor: "testDeliveryFlag" is false.
   *
   * @see #HeaderTypeFactory(String, String, String, String, boolean)
   */
  public HeaderTypeFactory(String manufacturer,
                           String productName,
                           String productVersion,
                           String messageType) {
    this(manufacturer, productName, productVersion, messageType, false);
  }

  /**
   * Create a message's header.
   * <p>
   * A new "messageId" is created with a random UUID.
   * </p>
   *
   * @param messageDate date to use as the "messageDate"
   *
   * @return a new instance of {@code HeaderType}
   */
  public HeaderType createHeader(LocalDateTime messageDate) {
    HeaderType header = new HeaderType();
    header.setSenderId(generateSenderId());
    header.setMessageId(UUID.randomUUID().toString());
    header.setMessageType(messageType);

    SendingApplicationType application = new SendingApplicationType();
    application.setManufacturer(manufacturer);
    application.setProduct(productName);
    application.setProductVersion(truncate(productVersion, 10));
    header.setSendingApplication(application);

    header.setMessageDate(messageDate);
    header.setAction(HEADER_ACTION);
    header.setTestDeliveryFlag(testFlag);

    return header;
  }

  /**
   * Like {@link #createHeader(LocalDateTime)} using {@code LocalDateTime.now()} as the "messageDate".
   *
   * @return a new instance of {@code HeaderType}
   */
  public HeaderType createHeader() {
    return createHeader(LocalDateTime.now());
  }

  private String generateSenderId() {
    // From ech 0058 documentation (translated) : "URI should be structured as :
    //     <issuer>://<Identification in the issuer's organization>"
    return "chvote://" + replaceWhitespaces(productName) + ":" + replaceWhitespaces(productVersion);
  }

  private static String replaceWhitespaces(String source) {
    return source.trim().replaceAll("(\\s)+", "_");
  }

  private static String truncate(String source, int maxLength) {
    if (source.length() <= maxLength) {
      return source;
    }
    return source.substring(0, maxLength);
  }

}
