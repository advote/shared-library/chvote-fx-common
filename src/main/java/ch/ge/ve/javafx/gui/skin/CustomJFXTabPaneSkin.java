/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-fx-common                                                                               -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.javafx.gui.skin;

import com.google.common.base.Preconditions;
import com.jfoenix.controls.JFXTabPane;
import com.jfoenix.skins.JFXTabPaneSkin;
import java.lang.reflect.Field;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.geometry.Side;
import javafx.scene.Node;
import javafx.scene.control.Tab;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Customization of the {@link JFXTabPaneSkin} needed to support bringing programmatically selected tabs into view.
 * <p>
 * The raw {@link JFXTabPaneSkin} does not move the header clip to bring the newly selected tab into view if it isn't
 * currently displayed. This class is intended as a workaround for this feature and might be removed if / when the
 * feature is supported by the original class.
 * <p>
 * As such, it was deemed more efficient (if less elegant) to implement this change using reflection and changing only
 * the relevant behaviour of the original class rather than reimplementing a specific Skin from scratch for a minor
 * change.
 */
public class CustomJFXTabPaneSkin extends JFXTabPaneSkin {
  private static final int    ANIMATION_DURATION = 500;
  private static final Logger log                = LoggerFactory.getLogger(CustomJFXTabPaneSkin.class);

  /**
   * @param tabPane the tab pane that will be skinned by this class
   */
  public CustomJFXTabPaneSkin(JFXTabPane tabPane) {
    super(tabPane);

    // Whenever the selection changes, we want to ensure the newly selected tab is visible in the header
    tabPane.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
      Tab selectedTab = tabPane.getTabs().get(newValue.intValue());
      this.scrollHeaderToViewTab(selectedTab);
    });
    tabPane.getSelectionModel().selectedItemProperty()
           .addListener((observable, oldValue, newValue) -> this.scrollHeaderToViewTab(newValue));
  }

  /**
   * Update the scrolling of the header so that the newly selected tab is visible, if needed.
   *
   * @param tab the tab that should be brought into the visible part of the srolling header
   */
  private void scrollHeaderToViewTab(Tab tab) {
    Preconditions.checkNotNull(tab);
    try {
      Class<?> superclass = getClass().getSuperclass();
      Field headerField = superclass.getDeclaredField("header");
      headerField.setAccessible(true);
      HeaderContainer header = (HeaderContainer) headerField.get(this);

      Field scrollOffsetField = header.getClass().getDeclaredField("scrollOffset");
      scrollOffsetField.setAccessible(true);
      Double originalScrollOffset = (Double) scrollOffsetField.get(header);

      Field regionField = header.getClass().getDeclaredField("headersRegion");
      regionField.setAccessible(true);
      StackPane headersRegion = (StackPane) regionField.get(header);

      Field clipField = header.getClass().getDeclaredField("clip");
      clipField.setAccessible(true);
      Rectangle clip = (Rectangle) clipField.get(header);

      NeedsScrollingResult needsScrollingResult = new NeedsScrollingResult(tab, headersRegion, clip).invoke();

      if (needsScrollingResult.isNeedsScrolling()) {
        scrollWithAnimation(header, originalScrollOffset, needsScrollingResult.getSelectedTabOffset());
      }
    } catch (NoSuchFieldException | IllegalAccessException e) {
      log.error("Couldn't scroll header into view", e);
    }
  }

  private void scrollWithAnimation(HeaderContainer header, Double originalScrollOffset, double selectedTabOffset) {
    DoubleProperty offsetProperty = new SimpleDoubleProperty(0);
    offsetProperty.addListener((o, oldVal, newVal) -> header.updateScrollOffset(newVal.doubleValue()));

    offsetProperty.set(originalScrollOffset);

    Timeline animation = new Timeline(new KeyFrame(Duration.millis(ANIMATION_DURATION),
                                                   new KeyValue(offsetProperty, -selectedTabOffset,
                                                                Interpolator.EASE_BOTH)));
    animation.play();
  }

  private class NeedsScrollingResult {
    private Tab       tab;
    private StackPane headersRegion;
    private Rectangle clip;
    private double    offset            = 0.0;
    private double    selectedTabOffset = 0.0;
    private boolean   needsScrolling    = false;

    NeedsScrollingResult(Tab tab, StackPane headersRegion, Rectangle clip) {
      this.tab = tab;
      this.headersRegion = headersRegion;
      this.clip = clip;
    }

    double getSelectedTabOffset() {
      return selectedTabOffset;
    }

    boolean isNeedsScrolling() {
      return needsScrolling;
    }

    NeedsScrollingResult invoke() throws NoSuchFieldException, IllegalAccessException {
      // Get the cumulative width of all previous tabs + the selected tab, in order to compute its position in the
      // header region.
      final Side side = getSkinnable().getSide();
      for (Node node : headersRegion.getChildren()) {
        if (node instanceof TabHeaderContainer) {
          TabHeaderContainer tabHeader = (TabHeaderContainer) node;
          double tabHeaderPrefWidth = snapSizeX(tabHeader.prefWidth(-1));
          Field tabField = tabHeader.getClass().getDeclaredField("tab");
          tabField.setAccessible(true);
          Tab loopTab = (Tab) tabField.get(tabHeader);
          if (tab.equals(loopTab)) {
            selectedTabOffset = (side == Side.LEFT || side == Side.BOTTOM) ?
                -offset - tabHeaderPrefWidth : offset;

            double minX = tabHeader.getBoundsInParent().getMinX();
            // We need to scroll if the tab is too far to the left (minX < 0)
            // or too far to the right (minX + element width) > width of the visible part of the header
            needsScrolling = minX < 0 || (minX + tabHeaderPrefWidth) > clip.getWidth();
            break;
          }
          offset += tabHeaderPrefWidth;
        }
      }
      return this;
    }
  }
}
