/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-fx-common                                                                               -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.javafx.gui.control;

import ch.ge.ve.javafx.gui.utils.FXMLLoaderUtils;
import com.google.common.base.Strings;
import de.jensd.fx.glyphs.materialicons.MaterialIcon;
import de.jensd.fx.glyphs.materialicons.MaterialIconView;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;

/**
 * A JavaFX element for displaying an inline message to the user. The message is styled using the provided {@link
 * MessageType} enum.
 */
public class MessageContainer extends HBox {
  private static final String      DEFAULT_STYLE_CLASS = "chvote-message-container";
  private static final MessageType DEFAULT_TYPE        = MessageType.INFO;
  private static final String      DEFAULT_FONT_SIZE   = "14px";

  @FXML
  private Label label;

  @FXML
  private MaterialIconView prefixIcon;

  private final ObjectProperty<MessageType> messageType;
  private final StringProperty              fontSize;

  /**
   * Create a new {@link MessageContainer} instance with an empty message. The type of the message will be {@link
   * MessageType#INFO} by default.
   */
  public MessageContainer() {
    this(null, DEFAULT_TYPE, DEFAULT_FONT_SIZE);
  }

  /**
   * Create a new {@link MessageContainer} for the given message and type.
   *
   * @param message     the message.
   * @param messageType the {@link MessageType}.
   * @param fontSize    the font size of the message.
   */
  public MessageContainer(String message, MessageType messageType, String fontSize) {
    this.messageType = new SimpleObjectProperty<>(this, "messageType");
    this.fontSize = new SimpleStringProperty(this, "fontSize");

    FXMLLoaderUtils.load(this, "/view/MessageConverter.fxml");

    initialize();

    setMessage(message);
    setMessageType(messageType);
    setFontSize(fontSize);
  }

  private void initialize() {
    getStyleClass().add(DEFAULT_STYLE_CLASS);

    messageTypeProperty().addListener((observable, oldValue, newValue) -> {
      prefixIcon.setIcon(newValue.getIcon());
      prefixIcon.setFill(newValue.getColor());
      label.setTextFill(newValue.getColor());
    });

    fontSizeProperty().addListener((observable, oldValue, newValue) -> {
      prefixIcon.setSize(newValue);
      label.setStyle(String.format("-fx-font-size: %s", newValue));
    });

    messageProperty().addListener((observable, oldValue, newValue) -> {
      boolean isVisible = !Strings.isNullOrEmpty(newValue);
      setManaged(isVisible);
      setVisible(isVisible);
    });

  }

  /**
   * Clear the message and hides the container. The {@link #fontSizeProperty} and {@link #messageTypeProperty} will be
   * not be modified.
   */
  public final void dismiss() {
    setMessage(null);
  }

  /**
   * An enum describing all possible types of messages.
   */
  public enum MessageType {
    /**
     * An information message (blue colored preceded by a {@link MaterialIcon#INFO} icon).
     */
    INFO(MaterialIcon.INFO, Color.valueOf("#457BA6")),
    /**
     * An error message (red colored preceded by a {@link MaterialIcon#ERROR} icon).
     */
    ERROR(MaterialIcon.ERROR, Color.valueOf("#D34336")),
    /**
     * A success message (green colored preceded by {@link MaterialIcon#DONE} icon).
     */
    SUCCESS(MaterialIcon.DONE, Color.valueOf("#4CAF50"));

    private final MaterialIcon icon;
    private final Color        color;

    MessageType(MaterialIcon icon, Color color) {
      this.icon = icon;
      this.color = color;
    }

    public MaterialIcon getIcon() {
      return icon;
    }

    public Color getColor() {
      return color;
    }
  }

  /*--------------------------------------------------------------------------
   - Properties                                                              -
   ---------------------------------------------------------------------------*/

  /**
   * Get the text of this message container.
   *
   * @return the text of this message container.
   */
  public final String getMessage() {
    return label.getText();
  }

  /**
   * Set the text of this message container. Setting the text to a null or empty string makes the container disappear,
   * setting to something else makes it visible.
   *
   * @param value the new text of this message container.
   */
  public final void setMessage(String value) {
    label.setText(value);
  }

  /**
   * The message property of this container.
   *
   * @return the message property of this container.
   */
  public final StringProperty messageProperty() {
    return label.textProperty();
  }

  /**
   * Get the type of message of this message container.
   *
   * @return the type of message.
   */
  public final MessageType getMessageType() {
    return messageType.get();
  }

  /**
   * Set the type of message of this message container.
   *
   * @param value the new type of message.
   */
  public final void setMessageType(MessageType value) {
    messageType.set(value);
  }

  /**
   * The message type property.
   *
   * @return the message type property.
   */
  public final ObjectProperty<MessageType> messageTypeProperty() {
    return messageType;
  }

  /**
   * Get the current font size.
   *
   * @return the current font size.
   */
  public final String getFontSize() {
    return fontSize.get();
  }

  /**
   * Set the font size for this message container. This effects both the text and the icon of this message
   * container. It is possible to add the units for the font size, e.g.: {@code 10px}, {@code 1em}...
   *
   * @param value the new font size for this message container.
   */
  public final void setFontSize(String value) {
    fontSize.setValue(value);
  }

  /**
   * The font size property.
   *
   * @return the font size property.
   */
  public final StringProperty fontSizeProperty() {
    return fontSize;
  }
}
