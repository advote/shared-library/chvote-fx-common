/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-fx-common                                                                               -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.javafx.gui.validator;

import com.google.common.base.Strings;
import com.jfoenix.validation.base.ValidatorBase;
import java.text.MessageFormat;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.TextInputControl;

/**
 * When this validator is added to a {@link TextInputControl} field it validates that its text is greater than or equal
 * to the given {@link #minLengthProperty()}}.
 */
public class TextFieldMinimumLengthValidator extends ValidatorBase {

  private final IntegerProperty minLength;
  private final StringProperty  baseMessage;

  /**
   * Create an empty text field minimum length validator.
   */
  public TextFieldMinimumLengthValidator() {
    this(null);
  }

  /**
   * Create a text field minimum length validator with the given default message.
   *
   * @param message the default message of this validator.
   */
  public TextFieldMinimumLengthValidator(String message) {
    super(message);

    this.minLength = new SimpleIntegerProperty(this, "minLength", 0);
    this.baseMessage = new SimpleStringProperty(this, "baseMessage");

    this.baseMessage.addListener((observable, oldValue, newValue) -> formatMessage());
    this.minLength.addListener((observable, oldValue, newValue) -> formatMessage());
  }

  private void formatMessage() {
    if (!Strings.isNullOrEmpty(getBaseMessage())) {
      this.setMessage(MessageFormat.format(getBaseMessage(), getMinLength()));
    }
  }

  @Override
  protected void eval() {
    if (srcControl.get() instanceof TextInputControl) {
      String text = ((TextInputControl) srcControl.get()).getText();
      hasErrors.set(Strings.isNullOrEmpty(text) || text.length() < getMinLength());
    }
  }

  /*--------------------------------------------------------------------------
   - Properties                                                              -
   ---------------------------------------------------------------------------*/

  /**
   * Set the expected minimum length.
   *
   * @param value the expected minimum length.
   */
  public final void setMinLength(Integer value) {
    minLength.set(value);
  }

  /**
   * Get the expected minimum length.
   *
   * @return the expected minimum length.
   */
  public final Integer getMinLength() {
    return minLength.get();
  }

  /**
   * Get the expected minimum length property.
   *
   * @return the expected minimum length property.
   */
  public final IntegerProperty minLengthProperty() {
    return minLength;
  }

  /**
   * Set the base message pattern. If this property is set the {@link #messageProperty()} of this validator will be
   * overridden using this pattern, the pattern must accept only one integer which corresponds to the {@link
   * #minLengthProperty()} of this instance.
   *
   * @param value the base message pattern.
   *
   * @see MessageFormat
   */
  public final void setBaseMessage(String value) {
    baseMessage.set(value);
  }

  /**
   * Get the base message pattern.
   *
   * @return the base message pattern.
   */
  public final String getBaseMessage() {
    return baseMessage.get();
  }

  /**
   * The base message pattern property.
   *
   * @return the base message pattern property.
   */
  public final StringProperty baseMessageProperty() {
    return baseMessage;
  }

}
