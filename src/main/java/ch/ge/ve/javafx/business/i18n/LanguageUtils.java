/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-fx-common                                                                               -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.javafx.business.i18n;

import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import javax.validation.constraints.NotNull;

/**
 * Utility class to interact with language related resources of a JavaFX Application, supported languages are {@link
 * Locale#FRENCH} and {@link Locale#GERMAN}, bundles under {@code i18n/resources} with the supported locales will be
 * automatically imported.
 */
public class LanguageUtils {
  private static final List<Locale> SUPPORTED_LOCALES = Arrays.asList(Locale.GERMAN, Locale.FRENCH);
  private static final Locale       DEFAULT_LOCALE    = Locale.FRENCH;

  /**
   * Get the current {@link Locale} of the application. It can only be one of these: {@link Locale#FRENCH} or {@link
   * Locale#GERMAN}. If at startup the system is set to another language, then this function will return {@link
   * Locale#FRENCH}.
   *
   * @return the current {@link Locale} of the application.
   */
  public static Locale getCurrentLocale() {
    if (!SUPPORTED_LOCALES.contains(Locale.getDefault())) {
      return DEFAULT_LOCALE;
    } else {
      return Locale.getDefault();
    }
  }

  /**
   * Set the {@link Locale} for the application, if the provided {@link Locale} is not a supported language, then {@link
   * #getCurrentLocale()} will return french.
   *
   * @param locale the new {@link Locale} to set globally.
   */
  public static void setCurrentLocale(Locale locale) {
    Locale.setDefault(locale);
  }

  /**
   * Get the {@link ResourceBundle} that matches the result of {@link #getCurrentLocale()}.
   *
   * @return The {@link ResourceBundle} that the application should use for the current {@link Locale}.
   *
   * @throws MissingResourceException it there is no bundle resource under {@code i18n/resources} for the current
   *                                  locale.
   */
  public static ResourceBundle getCurrentResourceBundle() {
    return new AggregateBundle(
        ResourceBundle.getBundle("i18n/fx_common", getCurrentLocale()),
        ResourceBundle.getBundle("i18n/resources", getCurrentLocale())
    );
  }

  private static class AggregateBundle extends ResourceBundle {
    private final Map<String, Object> content = new HashMap<>();

    AggregateBundle(ResourceBundle... bundles) {
      for (ResourceBundle bundle : bundles) {
        Enumeration keys = bundle.getKeys();

        while (keys.hasMoreElements()) {
          String oneKey = (String) keys.nextElement();
          if (!this.content.containsKey(oneKey)) {
            this.content.put(oneKey, bundle.getObject(oneKey));
          }
        }
      }
    }

    @Override
    protected Object handleGetObject(@NotNull String key) {
      return this.content.get(key);
    }

    @Override
    public Enumeration<String> getKeys() {
      return Collections.enumeration(this.content.keySet());
    }
  }


  /**
   * Hide utility class constructor.
   */
  private LanguageUtils() {
    throw new AssertionError("Not instantiable");
  }

}
