/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-fx-common                                                                               -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.javafx.business.xml

import java.time.LocalDateTime
import spock.lang.Specification

class HeaderTypeFactoryTest extends Specification {

  def "CreateHeader should create a header using the provided parameters"() {
    given:
    def factory = new HeaderTypeFactory("SIDP", "TestProduct", "1.0", "TEST-MSG", true)
    def instant = LocalDateTime.of(2018, 6, 21, 15, 5)

    when:
    def header = factory.createHeader(instant)

    then:
    header.senderId == "chvote://TestProduct:1.0"
    header.messageId =~ "^(\\w){8}-(\\w){4}-(\\w){4}-(\\w){4}-(\\w){12}\$"
    header.messageType == "TEST-MSG"

    header.sendingApplication.manufacturer == "SIDP"
    header.sendingApplication.product == "TestProduct"
    header.sendingApplication.productVersion == "1.0"

    header.messageDate == LocalDateTime.parse("2018-06-21T15:05:00.000")
    header.action == "1"
    header.testDeliveryFlag
  }

  def "CreateHeader should create a senderId based on product name and version and remove whitespaces"() {
    given:
    def productName = " abc  def \t gh "
    def productVersion = " 1.0 2018.06 "
    def factory = new HeaderTypeFactory("SIDP", productName, productVersion, "TEST-MSG")

    when:
    def header = factory.createHeader()

    then:
    header.senderId == "chvote://abc_def_gh:1.0_2018.06"
  }
}
