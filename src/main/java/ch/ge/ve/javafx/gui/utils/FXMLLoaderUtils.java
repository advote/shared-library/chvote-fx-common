/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-fx-common                                                                               -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.javafx.gui.utils;

import ch.ge.ve.javafx.business.i18n.LanguageUtils;
import ch.ge.ve.javafx.gui.utils.exception.CannotLoadResourceException;
import java.io.IOException;
import javafx.fxml.FXMLLoader;
import org.springframework.context.ApplicationContext;

/**
 * A FXML loading utility class.
 */
public class FXMLLoaderUtils {

  /**
   * Loads the given FXML document using the provided {@link ApplicationContext}.
   *
   * @param applicationContext the {@link ApplicationContext} containing the controller factory.
   * @param resourceLocation   the location of the FXNL document.
   * @param <T>                the type of the loaded document's root object.
   *
   * @return the loaded object hierarchy.
   *
   * @throws CannotLoadResourceException if an error occurs during loading.
   */
  public static <T> T load(ApplicationContext applicationContext, String resourceLocation) {
    FXMLLoader loader = new FXMLLoader();
    loader.setLocation(applicationContext.getClass().getResource(resourceLocation));

    loader.setResources(LanguageUtils.getCurrentResourceBundle());
    loader.setControllerFactory(applicationContext::getBean);

    try {
      return loader.load();
    } catch (IOException e) {
      throw new CannotLoadResourceException(
          String.format("An error occurred while trying to load resource at: [%s]", resourceLocation), e);
    }
  }

  /**
   * Loads the given FXML document using the provided controller.
   *
   * @param controller       the controller of the FXML document.
   * @param resourceLocation the location of the FXNL document.
   * @param <T>              the type of the loaded document's root object.
   *
   * @return the loaded object hierarchy.
   *
   * @throws CannotLoadResourceException if an error occurs during loading.
   */
  public static <T> T load(Object controller, String resourceLocation) {
    FXMLLoader loader = new FXMLLoader();
    loader.setLocation(controller.getClass().getResource(resourceLocation));
    loader.setResources(LanguageUtils.getCurrentResourceBundle());
    loader.setRoot(controller);
    loader.setController(controller);

    try {
      return loader.load();
    } catch (IOException e) {
      throw new CannotLoadResourceException(
          String.format("An error occurred while trying to load resource at: [%s]", resourceLocation), e);
    }
  }

  /**
   * Hide utility class constructor.
   */
  private FXMLLoaderUtils() {
    throw new AssertionError("Not instantiable");
  }

}
